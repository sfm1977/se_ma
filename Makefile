all: test run

test:
	@gotest -v ./...

run:
	@go run main.go