package routes

import (
	"fmt"
	"net/http"
	"text/template"
)

func LoginHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {

		render(w, "login.html", nil)

	} else {

		fmt.Println("Working on that")
	}

}

func render(w http.ResponseWriter, page string, data interface{}) {

	path := "web/views/"

	t := template.Must(template.New(page).ParseFiles(path + page))
	t.ExecuteTemplate(w, page, data)

}
