package main

import (
	"se_ma/routes"
	"log"
	"net/http"
	"time"
)

func main() {

	// NewServeMux implementes a multiplexer
	mux := http.NewServeMux()

	mux.HandleFunc("/login", routes.LoginHandler)

	mux.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("web/static"))))

	// Personalize the server, expectialy the timoute reading and writing
	s := &http.Server{
		Addr:         ":8081",
		Handler:      mux,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	log.Printf("Listning in port %s ...", s.Addr)
	log.Fatal(s.ListenAndServe())

}
